//
// Simple example to demonstrate how to imitate a smart battery.
//
// Author: Julian Oes <julian@oes.ch>

#include <mavsdk/mavsdk.h>
#include <mavsdk/plugins/mavlink_passthrough/mavlink_passthrough.h>
#include <mavsdk/plugins/mavlink_passthrough/mavlink/v2.0/common/mavlink_msg_servo_output_raw.h>
#include <mavsdk/plugins/telemetry/telemetry.h>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <future>
#include <memory>
#include <thread>

using namespace mavsdk;
using std::chrono::seconds;

static void subscribe_armed(Telemetry& telemetry);
static void send_battery_status(MavlinkPassthrough& mavlink_passthrough);

void usage(const std::string& bin_name)
{
    std::cerr << "Usage : " << bin_name << " <connection_url>\n"
              << "Connection URL format should be :\n"
              << " For TCP : tcp://[server_host][:server_port]\n"
              << " For UDP : udp://[bind_host][:bind_port]\n"
                  << " For Serial : serial:///path/to/serial/dev[:baudrate]\n"
              << "For example, to connect to the simulator use URL: udp://:14540\n";
}


std::shared_ptr<System> get_system(Mavsdk& mavsdk)
{
    std::cout << "Waiting to discover system...\n";
    auto prom = std::promise<std::shared_ptr<System>>{};
    auto fut = prom.get_future();

    // We wait for new systems to be discovered, once we find one that has an
    // autopilot, we decide to use it.
    mavsdk.subscribe_on_new_system([&mavsdk, &prom]() {
        auto system = mavsdk.systems().back();

        if (system->has_autopilot()) {
            std::cout << "Discovered autopilot\n";

            // Unsubscribe again as we only want to find one system.
            mavsdk.subscribe_on_new_system(nullptr);
            prom.set_value(system);
        }
    });



    // We usually receive heartbeats at 1Hz, therefore we should find a
    // system after around 3 seconds max, surely.
    if (fut.wait_for(seconds(3)) == std::future_status::timeout) {
        std::cerr << "No autopilot found.\n";
        return {};
    }

    // Get discovered system now.
    return fut.get();
}

int main(int argc, char** argv)
{


    Mavsdk mavsdk;
//    ConnectionResult connection_result = mavsdk.add_any_connection("serial:///dev/tty.usbmodem21201:115200");
    ConnectionResult connection_result = mavsdk.add_any_connection("udp://:14540");
//115200
    if (connection_result != ConnectionResult::Success) {
        std::cerr << "Connection failed: " << connection_result << '\n';
        return 1;
    }

    auto system = get_system(mavsdk);
    if (!system) {
        return 1;
    }

    // Instantiate plugins.
    auto telemetry = Telemetry{system};

    auto mavlink_passthrough = MavlinkPassthrough{system};



    std::function<void(const mavlink_message_t &)> m_callback;

    m_callback = [](const mavlink_message_t &msg_raw)
    {

        mavlink_servo_output_raw_t  servo_output_raw;

        mavlink_msg_servo_output_raw_decode(&msg_raw, &servo_output_raw);


        // Extract PWM value for a specific motor (change index as per your setup)

        std::cout <<"PWM port!!!"<< servo_output_raw.port;
        std::cout <<"PWM servo1_raw !!!"<< servo_output_raw.servo1_raw;



    };
    mavlink_passthrough.subscribe_message_async(MAVLINK_MSG_ID_SERVO_OUTPUT_RAW, m_callback);

    subscribe_armed(telemetry);

    while (true) {
        send_battery_status(mavlink_passthrough);
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    // Subscribe to "SERVO_OUTPUT_RAW" messages


    return 0;
}


void subscribe_armed(Telemetry& telemetry)
{
    telemetry.subscribe_armed(
            [](bool is_armed) { std::cout << (is_armed ? "armed" : "disarmed") << '\n'; });
//    telemetry.subscribe_imu(
//            [](Telemetry::Imu imu) { std::cout << (imu.acceleration_frd.down_m_s2) << '\n'; });

    telemetry.subscribe_raw_imu([](Telemetry::Imu imu) { std::cout <<"angular_velocity_frd.down_rad_s"<< (imu.angular_velocity_frd.down_rad_s) << '\n'; });
    telemetry.subscribe_raw_imu([](Telemetry::Imu imu) { std::cout << (imu.angular_velocity_frd) << '\n'; });
//    telemetry.subscribe_raw_imu([](Telemetry::Imu imu) { std::cout << (imu.acceleration_frd.down_m_s2) << '\n'; });





}

void send_battery_status(MavlinkPassthrough& mavlink_passthrough)
{
    const uint16_t voltages[10]{
            3700,
            3600,
            std::numeric_limits<uint16_t>::max(),
            std::numeric_limits<uint16_t>::max(),
            std::numeric_limits<uint16_t>::max(),
            std::numeric_limits<uint16_t>::max(),
            std::numeric_limits<uint16_t>::max(),
            std::numeric_limits<uint16_t>::max(),
            std::numeric_limits<uint16_t>::max(),
            std::numeric_limits<uint16_t>::max()}; // mV

    const uint16_t voltages_ext[4]{0, 0, 0, 0};

    mavlink_message_t message;
    mavlink_msg_battery_status_pack(
            mavlink_passthrough.get_our_sysid(),
            mavlink_passthrough.get_our_compid(),
            &message,
            0, // id
            MAV_BATTERY_FUNCTION_ALL, // battery_function
            MAV_BATTERY_TYPE_LION, // type
            2500, // 100*temperature C
            &voltages[0],
            4000, // 100*current_battery A
            1000, // current_consumed, mAh
            -1, // energy consumed hJ
            80, // battery_remaining %
            3600, // time_remaining
            MAV_BATTERY_CHARGE_STATE_OK,
            voltages_ext,
            MAV_BATTERY_MODE_UNKNOWN, // mode
            0); // fault_bitmask

    mavlink_passthrough.send_message(message);
}
